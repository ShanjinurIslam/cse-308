package RentShop;

abstract class Dress {
    String shirt_type ;
    String pant_type ;

    abstract int getRentPrice() ;

}

class Formal extends Dress{
    Formal(){
        this.shirt_type  = "Shirt" ;
        this.pant_type = "Pant" ;
    }

    int getRentPrice(){
        return 1300 ;
    }
}


class Casual extends Dress{
    Casual(){
        this.shirt_type  = "T-Shirt" ;
        this.pant_type = "Jeans" ;
    }

    int getRentPrice(){
        return 1300 ;
    }
}

