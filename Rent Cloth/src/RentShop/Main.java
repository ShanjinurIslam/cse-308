package RentShop;

public class Main {
    public static void main(String args[]){
        RestShop restShop = new RestShop(3,3,2,2) ;
        Order order = restShop.placeOrder("Casual",1,1) ;
        if(order!=null){
            System.out.println("Total Bill: " + order.getBill()) ;
        }
    }
}
