package RentShop;

public class RestShop {
    private int formal ;
    private int casual ;
    private int walletCount ;
    private int smartWatchCount ;

    public RestShop(int f,int c,int w,int sw){
        this.formal =f ;
        this.casual =c ;
        this.walletCount = w ;
        this.smartWatchCount = sw ;
    }

    Order placeOrder(String dressType,int Wallet,int SmartWatch){
        if(dressType.equalsIgnoreCase("Formal")){
            if(formal>=1){
                formal-- ;
                if(walletCount>=1){
                    walletCount -= Wallet ;
                }
                else{
                    Wallet = 0 ;
                    System.out.println("Wallet Out of stock");
                }
                if(smartWatchCount>=1){
                    smartWatchCount -= SmartWatch ;
                }
                else{

                    System.out.println("SmartWatch Out of stock");
                    SmartWatch = 0 ;
                }
                return new Order(dressType,Wallet,SmartWatch) ;
            }
            else{
                System.out.println("Formal Out of stock");
                return null ;
            }
        }
        else if(dressType.equalsIgnoreCase("Casual")){
            if(casual>=1){
                casual-- ;
                if(walletCount>=1){
                    walletCount -= Wallet ;
                }
                else{
                    Wallet=  0 ;
                    System.out.println("Wallet Out of stock");
                }
                if(smartWatchCount>=1){
                    smartWatchCount -= SmartWatch ;
                }
                else{
                    SmartWatch = 0 ;
                    System.out.println("SmartWatch Out of stock");
                }
                return new Order(dressType,Wallet,SmartWatch) ;
            }
            else{
                System.out.println("Out of stock");
                return null ;
            }
        }
        return null ;
    }
}
