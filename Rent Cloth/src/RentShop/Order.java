package RentShop;

public class Order {
    private Dress dress ;
    private int Wallet ;
    private int SmartWatch ;

    Order(String dress_type,int wallet,int smartWatch){
        if(dress_type=="Formal"){
            dress = new Formal() ;
        }
        if(dress_type=="Casual"){
            dress = new Casual() ;
        }
        Wallet = wallet ;
        SmartWatch = smartWatch ;
    }

    int getBill(){
        return dress.getRentPrice() + (Wallet*200) + (SmartWatch*500) ;
    }
}
