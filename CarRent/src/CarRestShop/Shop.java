package CarRestShop;

public class Shop {
    int pc ;
    int mc ;
    Car car ;

    Shop(int pc,int mc){
        this.pc = pc ;
        this.mc = mc ;
    }

    int rentCar(int count, int aircon, int tools) {
        if (count > 4) {
            if(pc>=1){
                car = new Micro() ;
                pc-- ;
            }
        } else {
            if(mc>=1){
                car = new Private() ;
                pc-- ;
            }
        }

        int totalRent = car.getBill() + (aircon * 500) + (tools * 500);

        return totalRent ;
    }
}
