package CarRestShop;

abstract class Car {
    int numberofSeating ;
    String name ;
    int rent_price ;

    abstract int getBill() ;
}

class Private extends Car{
    Private(){
        numberofSeating = 4 ;
        name = "Private Car" ;
        rent_price = 4000 ;
    }

    int getBill(){
        return rent_price ;
    }
}

class Micro extends Car{
    Micro(){
        numberofSeating = 6 ;
        name = "Private Car" ;
        rent_price = 5500 ;
    }

    int getBill(){
        return rent_price ;
    }
}